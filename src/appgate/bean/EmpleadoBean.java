package appgate.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import appgate.entidades.Empleado;
import appgate.servicio.IEmpleadosServicio;

/**
 * Managed Bean de la p�gina empleado.xhtml.
 * Esta p�gina se encarga de ingresar la informaci�n de los empleados.
 * @author Luis Fernando Martinez Galvez 
 */
@ManagedBean
@ViewScoped
public class EmpleadoBean implements Serializable {
	
	/**
	 * Variable de serializacion
	 */
	private static final long serialVersionUID = 7457169225823098127L;
	
	/**
	 * Se agrega variable de log
	 */
	protected static final Logger LOGGER = Logger.getLogger(EmpleadoBean.class);
	
	/**
	 * Interface del servicio de Almacenes
	 */
	@EJB
	private IEmpleadosServicio empleadoServicio;
		
	/**
	 * Objeto almacenes
	 */
	private Empleado empleado;
				
	@PostConstruct
	public void inicializarVariables() {		
		empleado = new Empleado();
	}

	/**
	 * Constructor por defecto
	 */	
	public EmpleadoBean() {
		
	}
			
	/**
	 * Permite guardar el almacen
	 */
	public void guardarEmpleado() {
		try {							
			empleadoServicio.guardarEmpleado(empleado);
			
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					 "El empleado se guard� satisfactoriamente",
					 "El empleado se guard� satisfactoriamente");
			FacesContext.getCurrentInstance().addMessage("messages", facesMsg); 
			limpiarEmpleado();
		} catch (RuntimeException re) {
        	LOGGER.error(re);
			re.printStackTrace();
		}catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();	
		}
		
	}
			
	/**
	 * Limpia la informacion del empleado
	 */
	public void limpiarEmpleado() {
		empleado = new Empleado();		
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}			
}