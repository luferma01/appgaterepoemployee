package appgate.servicio.impl;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import appgate.entidades.Empleado;
import appgate.servicio.EntityManagerServicio;
import appgate.servicio.IEmpleadosServicio;

/**
 * Clase que permite persistir objetos en la base de datos
 * @author Luis Fernando Martinez Galvez 
 */	
@Stateless
public class EmpleadosServicio extends EntityManagerServicio implements IEmpleadosServicio {
			
	/**
	 * Constructor
	 */
	public EmpleadosServicio() {	
	}
			
	/**
	 * Constante que representa la instancia del Log
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(EmpleadosServicio.class);
					
	@Override
	public Empleado guardarEmpleado(Empleado empleado){
		try {
			entityManager.getTransaction().begin();
			if(empleado.getIdEmpleado()==null){
				entityManager.persist(empleado);
			}else{
				entityManager.merge(empleado);
			}
			entityManager.getTransaction().commit();			
		} catch (RuntimeException e) {
			entityManager.getTransaction().rollback();
			LOG.info("Fin guardar empleado("
					+ empleado.toString() + ")");			
		}
		return empleado;
	}	
}