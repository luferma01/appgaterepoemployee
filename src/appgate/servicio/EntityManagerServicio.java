package appgate.servicio;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

/**
 * Clase donde se instancia el entity manager
 * @author Luis Fernando Martinez Galvez
 */
@Stateless
public class EntityManagerServicio {
			
	/**
	 * Entidad de persistencia inyectada
	 */
	@PersistenceContext(unitName="appGate", type= PersistenceContextType.TRANSACTION)
    public EntityManager entityManager;

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
			
}