package appgate.servicio;

import appgate.entidades.Empleado;


/**
 * Interface para las operaciones con las clases 
 */
public interface IEmpleadosServicio {
					 		
	/**
	  * Permite guardar los empleados
	  * @param objeto empleado
	  * @return objeto empleado guardado
	  * @throws Exception excepcion
	  */
	Empleado guardarEmpleado(Empleado empleado) throws Exception;	
}