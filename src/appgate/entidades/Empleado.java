package appgate.entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Empleado" database table.
 * 
 */
@Entity
@Table(name="public.empleado")
@NamedQuery(name="Empleado.findAll", query="SELECT e FROM Empleado e")
public class Empleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EMPLEADO_IDEMPLEADO_GENERATOR", sequenceName="SEQ_ID_EMPLEADO", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPLEADO_IDEMPLEADO_GENERATOR")
	@Column(name="idempleado")
	private Integer idEmpleado;

	@Column(name="meses")
	private Integer meses;

	@Column(name="nombre")
	private String nombre;

	@Column(name="salario")
	private Integer salario;

	public Empleado() {
	}

	public Integer getIdEmpleado() {
		return this.idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Integer getMeses() {
		return this.meses;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getSalario() {
		return this.salario;
	}

	public void setSalario(Integer salario) {
		this.salario = salario;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;		
		result = prime * result
				+ ((idEmpleado == null) ? 0 : idEmpleado.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;		
		if (idEmpleado == null) {
			if (other.idEmpleado != null)
				return false;
		} else if (!idEmpleado.equals(other.idEmpleado))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Empleados [idEmpleado=" + idEmpleado + "]";
	}	
}