# README #

Project name: appGateDevelopProject

### Project basic information ###

* In this Git repository is stored the source code of the appgate develop project
* Version 1.0

### Technical information and configuration project ###

1. Source code structure:
	* The project structure is based on the model-view-controller design pattern
    * src/appgate/bean: Bean class that allows interact with the xhtml web page
    * src/appgate/entidades: Entities related to the data base tables
    * src/appgate/servicio: Interface where are defined the methods to connect with the data base
    * src/appgate/servicio/impl: In the class are created the methods to interact with the data base
    * resources: Is created a property file to define variables to connect with the data base
    * META-INF: Is created the persistence.xml file with the mapping of the data base tables
    * WebContent/paginas/empleado.xhtml: Xhtml web page with the form view, this page contains three fields and one logo image of appgate company
    * WebContent/resources/
        * css: Are created styles to use throughout the application
        * Imagenes: Are saved images to use throughout the application
2. Was created 1 web page with the basic fields to save the the employee information
3. Libraries: The libraries should be loaded in a locally way to execute the project
4. Web server: The used web server is "Red Hat JBoss EAP 6.3" the persistence configuration was donde in the "standalone-full.xml" file
5. Git repository: Was created a public git repository by using bitbucket with this command: git clone https://bitbucket.org/luferma01/appgaterepoemployee.git